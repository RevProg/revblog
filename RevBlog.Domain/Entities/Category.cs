﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Web.Mvc;

namespace RevBlog.Domain.Entities
{
    public class Category
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Алиас")]
        [StringLength(50)]
        public string Alias { get; set; }

        [Display(Name = "Родительская категория")]
        public int? ParentID { get; set; }

        public virtual Category Parent { get; set; }

        [ForeignKey("ParentID")]
        public virtual ICollection<Category> Children { get; set; }

        [Display(Name = "Описание")]
        [DataType(DataType.Text)]
        [AllowHtml]
        public string Description { get; set; }

        [Display(Name = "Мета описание")]
        [DataType(DataType.Text)]
        public string MetaDescription { get; set; }

        [Display(Name = "Мета ключевые слова")]
        [DataType(DataType.Text)]
        public string MetaKeyWords { get; set; }

        [Display(Name = "Активна?")]
        public bool isActive { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
