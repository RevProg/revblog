﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RevBlog.Domain.Entities
{
    public class Tag
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set; }
    }
}
