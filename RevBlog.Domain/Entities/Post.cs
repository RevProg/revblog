﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RevBlog.Domain.Entities
{
    public class Post
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Алиас")]
        public string Alias { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [AllowHtml]
        [Display(Name = "Краткое содержание")]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [AllowHtml]
        [Display(Name = "Содержание")]
        public string Content { get; set; }

        [DataType(DataType.Text)]
        public string MetaDescription { get; set; }

        [DataType(DataType.Text)]
        public string MetaKeyWords { get; set; }

        [Display(Name = "Опубликовано?")]
        public bool isActive { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Дата добавления")]
        public DateTime Created { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int CategoryID { get; set; }

        public Category Category { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
