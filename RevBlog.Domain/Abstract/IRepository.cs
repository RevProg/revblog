﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevBlog.Domain.Abstract
{
    public interface IRepository<T>
    {
        IQueryable<T> List { get; }

        Task<T> FindAsync(int id);

        Task SaveAsync(T item);

        Task<T> DeleteAsync(int id);
    }
}
