﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevBlog.Domain.Entities;

namespace RevBlog.Domain.Abstract
{
    public interface ICategoryRepository: IRepository<Category>
    {
    }
}
