﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using System.Data.Entity;

namespace RevBlog.Domain.Concrete
{
    public class CategoryRepository : ICategoryRepository
    {
        private BlogContext context = new BlogContext();

        public IQueryable<Category> List
        {
            get
            {
                return context.Categories;    
            }
        }

        public async Task<Category> FindAsync(int id)
        {
            return await context.Categories.SingleOrDefaultAsync(c => c.ID == id);
        }

        public async Task SaveAsync(Category item)
        {
           if (item.ID == 0)
           {
               context.Categories.Add(item);
           }
           else
           {
               context.Entry(item).State = EntityState.Modified;
           }

           await context.SaveChangesAsync();
        }

        public async Task<Category> DeleteAsync(int id)
        {
           Category category = await context.Categories.SingleOrDefaultAsync(c => c.ID == id);

           if (category != null)
           {
               context.Categories.Remove(category);
               await context.SaveChangesAsync();
           }

           return category;
        }
    }
}
