﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using System.Data.Entity;

namespace RevBlog.Domain.Concrete
{
    public class TagRepository: ITagRepository
    {
        private BlogContext context = new BlogContext();

        public IQueryable<Tag> List
        {
            get
            {
                return context.Tags;
            }
        }

        public async Task<Tag> FindAsync(int id)
        {
            return await context.Tags.SingleOrDefaultAsync(c => c.ID == id);
        }

        public async Task<Tag> FindByNameAsync(string name)
        {
            return await context.Tags.SingleOrDefaultAsync(c => c.Name.ToLower() == name.ToLower());
        }

        public async Task<Tag> FindWithPostsAsync(int id)
        {
            return await context.Tags.Include(t => t.Posts).SingleOrDefaultAsync(c => c.ID == id);
        }

        public async Task SaveAsync(Tag item)
        {
            if (item.ID == 0)
            {
                context.Tags.Add(item);
            }
            else
            {
                context.Entry(item).State = EntityState.Modified;
            }

            await context.SaveChangesAsync();
        }

        public async Task<Tag> DeleteAsync(int id)
        {
            Tag tag = await context.Tags.SingleOrDefaultAsync(c => c.ID == id);

            if (tag != null)
            {
                context.Tags.Remove(tag);
                await context.SaveChangesAsync();
            }

            return tag;
        }
    }
}
