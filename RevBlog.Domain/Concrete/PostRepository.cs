﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using System.Data.Entity;

namespace RevBlog.Domain.Concrete
{
    public class PostRepository : IPostRepository
    {
        private BlogContext context = new BlogContext();

        public IQueryable<Post> List
        {
            get
            {
                return context.Posts;
            }
        }

        public async Task<Post> FindAsync(int id)
        {
            return await context.Posts.Where(p => p.ID == id).Include(p => p.Category).SingleOrDefaultAsync();
        }

        public async Task<Post> FindWithTagsAsync(int id)
        {
            return await context.Posts.Where(p => p.ID == id).Include(p => p.Category).Include(p => p.Tags).SingleOrDefaultAsync();
        }

        public async Task SaveAsync(Post item)
        {
            if (item.ID == 0)
            {
                context.Posts.Add(item);
            }
            else
            {
                context.Entry(item).State = EntityState.Modified;

                if (item.Tags != null)
                {
                    foreach(Tag tag in item.Tags)
                    {
                        context.Entry(tag).State = tag.ID == 0 ? EntityState.Added : EntityState.Unchanged;
                    }
                }
            }

            await context.SaveChangesAsync();
        }

        public async Task<Post> DeleteAsync(int id)
        {
            Post post = await FindAsync(id);

            if (post != null)
            {
                context.Posts.Remove(post);
                await context.SaveChangesAsync();
            }

            return post;
        }
    }
}
