﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using RevBlog.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace RevBlog.Domain.Concrete
{
    public class BlogContext: IdentityDbContext<ApplicationUser>
    {
        public BlogContext() : base("BlogConnection")
        {
            Database.SetInitializer<BlogContext>(new CreateDatabaseIfNotExists<BlogContext>());
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public static BlogContext Create()
        {
            return new BlogContext();
        }
    }
}