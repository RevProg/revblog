﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using RevBlog.Web.Controllers;
using Moq;
using System.Linq;

namespace RevBlog.Tests.Controllers
{
    [TestClass]
    public class CategoryControllerTest
    {
        [TestMethod]
        public void Generate_Active_Categories_List()
        {
            //arrange
            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();
            mock.Setup(m => m.List).Returns(new Category[] {
                new Category { ID = 1, Name = "First", isActive = true },
                new Category { ID = 2, Name = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", isActive = false },
                new Category { ID = 4, Name = "Fourth", isActive = true },
            }.AsQueryable());

            //act
            CategoryController controller = new CategoryController(mock.Object);

            PartialViewResult result = controller.List();

            var model = result.Model as IQueryable<Category>;

            //assert
            Assert.AreEqual(3, model.Count());
        }
    }
}
