﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using RevBlog.Web.Models;
using RevBlog.Web.Controllers;
using Moq;
using System.Web.Mvc;
using RevBlog.Web.Extensions;
using System.Web;
using System.Web.Routing;
using System.IO;
using RevBlog.Tests.Components;

namespace RevBlog.Tests.Controllers
{
    [TestClass]
    public class BlogControllerTest
    {
        [TestMethod]
        public void Can_Paginate()
        {
            //arrange
            Mock<IPostRepository> mock = new Mock<IPostRepository>();
            mock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", isActive = true },
            }.AsQueryable());

            BlogController controller = new BlogController(mock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.ItemsPerPage = 3;

            //act
            PostListViewModel result = (PostListViewModel)(controller.Index(2) as ViewResult).Model;

            //assert
            Post[] posts = result.Posts.ToArray();
            Assert.IsTrue(posts.Length == 1);
            Assert.AreEqual("First", posts[0].Title);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(2, controller.ViewBag.breadCrumb.Count);
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Arrange - define an HTML helper - we need to do this
            // in order to apply the extension method
            HtmlHelper myHelper = null;

            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            Func<int, string> pageUrlDelegate = i => "Page" + i;

            //act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            //assert
            Assert.AreEqual(@"<ul class=""pagination"">"
                            + @"<li><a href=""Page1"">1</a></li>"
                            + @"<li class=""active""><a href=""Page2"">2</a></li>"
                            + @"<li><a href=""Page3"">3</a></li></ul>",
                             result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            //arrange
            Mock<IPostRepository> mock = new Mock<IPostRepository>();
            mock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", isActive = true },
            }.AsQueryable());

            BlogController controller = new BlogController(mock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.ItemsPerPage = 3;

            //act 
            PostListViewModel result = (PostListViewModel)(controller.Index(2) as ViewResult).Model;

            //assert
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(2, pageInfo.CurrentPage);
            Assert.AreEqual(3, pageInfo.ItemsPerPage);
            Assert.AreEqual(4, pageInfo.TotalItems);
            Assert.AreEqual(2, pageInfo.TotalPages);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(2, controller.ViewBag.breadCrumb.Count);
        }

        [TestMethod]
        public void Generate_Category_Specific_Product_Count()
        {
            //arrange
            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", CategoryID = 2, isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", CategoryID = 3, isActive = true },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", CategoryID = 1, isActive = true },
            }.AsQueryable());

            Mock<ICategoryRepository> catMock = new Mock<ICategoryRepository>();
            catMock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(new Category { ID = 1, Name = "First", isActive = true }); });

            BlogController controller = new BlogController(postMock.Object, catMock.Object, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();
            controller.ItemsPerPage = 10;

            //act 
            PostListViewModel result = (PostListViewModel)(controller.Category(1, "First").Result as ViewResult).Model;

            //assert
            PagingInfo pageInfo = result.PagingInfo;

            Assert.AreEqual(2, pageInfo.TotalItems);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(3, controller.ViewBag.breadCrumb.Count);
        }

        [TestMethod]
        public void Generate_List_By_Tag()
        {
            //arrange
            Tag tag = new Tag { ID = 1, Name = "Tag1" };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, isActive = true, Tags = new List<Tag> { tag } },
                new Post { ID = 2, Title = "Second", Alias = "Second", CategoryID = 2, isActive = true, Tags = new List<Tag>() },
                new Post { ID = 3, Title = "Third", Alias = "Third", CategoryID = 3, isActive = true, Tags = new List<Tag> { tag } },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", CategoryID = 1, isActive = true, Tags = new List<Tag>() },
            }.AsQueryable());

            Mock<ITagRepository> tagMock = new Mock<ITagRepository>();
            tagMock.Setup(m => m.FindByNameAsync(It.IsAny<string>())).Returns(Task.FromResult(tag));

            BlogController controller = new BlogController(postMock.Object, null, tagMock.Object);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();
            controller.ItemsPerPage = 10;

            //act 
            PostListViewModel result = (PostListViewModel)(controller.Tag(tag.Name).Result as ViewResult).Model;

            //assert
            PagingInfo pageInfo = result.PagingInfo;

            List<Post> posts = result.Posts.ToList();

            Assert.AreEqual(2, pageInfo.TotalItems);
            Assert.AreEqual(3, posts[0].ID);
            Assert.AreEqual(1, posts[1].ID);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(3, controller.ViewBag.breadCrumb.Count);
        }

        [TestMethod]
        public void View_Post_Details()
        {
            //arrange
            Tag tag = new Tag { ID = 1, Name = "Tag1" };
            Category category = new Category { ID = 1, Name = "First", Alias = "First" };
            Post post = new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, Category = category, isActive = true, Tags = new List<Tag> { tag } };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns((int id) => (id == post.ID) ? Task.FromResult(post) : null);

            BlogController controller = new BlogController(postMock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();

            //act 
            Post result = (controller.Details(post.ID, post.Alias).Result as ViewResult).Model as Post;

            Assert.AreEqual(1, result.ID);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(4, controller.ViewBag.breadCrumb.Count);
        }

        [TestMethod]
        public void View_Non_Active_Post_Details()
        {
            //arrange
            Tag tag = new Tag { ID = 1, Name = "Tag1" };
            Category category = new Category { ID = 1, Name = "First", Alias = "First" };
            Post post = new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, Category = category, isActive = false, Tags = new List<Tag> { tag } };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns(Task.FromResult<Post>(null));

            BlogController controller = new BlogController(postMock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();

            //act 
            var result = controller.Details(1, "First").Result;
            int code = (result as HttpNotFoundResult).StatusCode;

            Assert.AreEqual(404, code);
            Assert.IsNull(controller.ViewBag.breadCrumb);
        }

        [TestMethod]
        public void View_Incorect_Post_Details()
        {
            //arrange
            Post post = null;

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns((int id) => Task.FromResult(post));

            BlogController controller = new BlogController(postMock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();

            //act 
            var result = controller.Details(33, "incorect").Result;
            int code = (result as HttpNotFoundResult).StatusCode;

            Assert.AreEqual(404, code);
            Assert.IsNull(controller.ViewBag.breadCrumb);
        }

        [TestMethod]
        public void View_Incorect_Alias_Post_Details()
        {
            //arrange
            Tag tag = new Tag { ID = 1, Name = "Tag1" };
            Category category = new Category { ID = 1, Name = "First", Alias = "First" };
            Post post = new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, Category = category, isActive = true, Tags = new List<Tag> { tag } };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns((int id) => Task.FromResult(post));

            BlogController controller = new BlogController(postMock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();

            //act 
            var result = controller.Details(1, "incorect").Result;
            int code = (result as HttpNotFoundResult).StatusCode;

            Assert.AreEqual(404, code);
            Assert.IsNull(controller.ViewBag.breadCrumb);
        }

        [TestMethod]
        public void Generate_Active_Last_Posts_List()
        {
            //arrange
            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", CategoryID = 1, isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", CategoryID = 2, isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", CategoryID = 3, isActive = false },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", CategoryID = 1, isActive = true },
            }.AsQueryable());

            //act
            BlogController controller = new BlogController(postMock.Object, null, null);

            PartialViewResult result = controller.LastPostsList();

            var model = result.Model as IQueryable<Post>;

            //assert
            Assert.AreEqual(3, model.Count());
        }

        [TestMethod]
        public void Can_Search()
        {
            //arrange
            Mock<IPostRepository> mock = new Mock<IPostRepository>();
            mock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "Yii2 application", Alias = "First", Description = "All about this framework", isActive = true },
                new Post { ID = 2, Title = "Module architecture", Alias = "Second", Description = "About yii2", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", Description = "Third", isActive = true },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", Description = "Fourth", isActive = true },
            }.AsQueryable());

            BlogController controller = new BlogController(mock.Object, null, null);
            controller.ControllerContext = MoqComponents.GetContext(controller);
            controller.Url = MoqComponents.GetUrl();
            controller.ItemsPerPage = 5;

            //act
            SearchListViewModel result = (SearchListViewModel)(controller.Search("yii") as ViewResult).Model;

            //assert
            Post[] posts = result.Posts.ToArray();
            Assert.IsTrue(posts.Length == 2);
            Assert.AreEqual("Module architecture", posts[0].Title);
            Assert.AreEqual("Yii2 application", posts[1].Title);
            Assert.IsNotNull(controller.ViewBag.breadCrumb);
            Assert.AreEqual(3, controller.ViewBag.breadCrumb.Count);
        }
    }
}
