﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RevBlog.Web;
using RevBlog.Web.Controllers;
using Moq;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;

namespace RevBlog.UnitTests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            //arrange
            Mock<IPostRepository> mock = new Mock<IPostRepository>();
            mock.Setup(m => m.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
                new Post { ID = 4, Title = "Fourth", Alias = "Fourth", isActive = true },
            }.AsQueryable());


            HomeController controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;
            List<Post> model = result.Model as List<Post>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, model.Count);
            Assert.AreEqual(4, model[0].ID);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController(null);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController(null);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
