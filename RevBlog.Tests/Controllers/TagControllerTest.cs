﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using RevBlog.Web.Controllers;
using Moq;
using System.Linq;
using System.Collections.Generic;

namespace RevBlog.Tests.Controllers
{
    [TestClass]
    public class TagControllerTest
    {
        [TestMethod]
        public void Generate_Ordered_By_Count_Tag_List()
        {
            //arrange
            Mock<ITagRepository> mock = new Mock<ITagRepository>();
            mock.Setup(m => m.List).Returns(new Tag[] {
                new Tag { ID = 1, Name = "First", Posts = new List<Post> { new Post { ID = 1, Title = "First" }} },
                new Tag { ID = 2, Name = "Second", Posts = new List<Post> { new Post { ID = 1, Title = "First" }, new Post { ID = 2, Title = "Second" } } },
                new Tag { ID = 3, Name = "Third", Posts = new List<Post>() }
            }.AsQueryable());

            //act
            TagController controller = new TagController(mock.Object);

            PartialViewResult result = controller.List();

            var model = (result.Model as IQueryable<Tag>).ToList();

            //assert
            Assert.AreEqual(3, model.Count());
            Assert.AreEqual(2, model[0].ID);
            Assert.AreEqual(1, model[1].ID);
            Assert.AreEqual(3, model[2].ID);
        }
    }
}
