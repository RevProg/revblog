﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using RevBlog.Web.Areas.Admin.Controllers;
using Moq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace RevBlog.Tests.Admin.Controllers
{
    [TestClass]
    public class CategoryControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            //Arrange
            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();
            mock.Setup(r => r.List).Returns(new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            }.AsQueryable<Category>());

            //Act

            CategoryController target = new CategoryController(mock.Object);

            Category[] result = ((IEnumerable<Category>) (target.Index() as ViewResult).ViewData.Model).ToArray();

            //Assert

            Assert.AreEqual(result[0].Name, "First");
            Assert.AreEqual(result[1].Name, "Second");
            Assert.AreEqual(result[2].Name, "Third");
            Assert.AreEqual(result.Length, 3);
        }

        [TestMethod]
        public void Can_Edit_Category()
        {
            Category[] categories = new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            };

            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();
            mock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(categories[id - 1]); });

            CategoryController target = new CategoryController(mock.Object);

            var result1 = target.Edit(1).Result;
            Category c1 = (result1 as ViewResult).ViewData.Model as Category;

            var result2 = target.Edit(2).Result;
            Category c2 = (result2 as ViewResult).ViewData.Model as Category;

            var result3 = target.Edit(3).Result;
            Category c3 = (result3 as ViewResult).ViewData.Model as Category;


            // Assert
            Assert.AreEqual(1, c1.ID);
            Assert.AreEqual(2, c2.ID);
            Assert.AreEqual(3, c3.ID);
        }

        [TestMethod]
        public void Cannot_Edit_Non_Existing_Category()
        {
            Category[] categories = new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            };

            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();
            mock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(categories.ElementAtOrDefault(id - 1)); });

            CategoryController target = new CategoryController(mock.Object);

            var result1 = target.Edit(4).Result;
            int code = (result1 as HttpNotFoundResult).StatusCode;

            Assert.AreEqual(404, code);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {

            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();

            CategoryController target = new CategoryController(mock.Object);

            Category category = new Category { ID=1, Name="First", Alias="First" };

            var context = new ValidationContext(category, null, null);

            var results = new List<ValidationResult>();
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Category), typeof(Category)), typeof(Category));
            var isModelStateValid = Validator.TryValidateObject(category, context, results, true);

            if (!isModelStateValid)
                target.ModelState.AddModelError("", "error");

            ActionResult result = target.Edit(category).Result;
            
            mock.Verify(m => m.SaveAsync(category));
            Assert.IsTrue(isModelStateValid);
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cant_Save_Invalid_Changes()
        {

            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();

            CategoryController target = new CategoryController(mock.Object);

            Category category = new Category();

            var context = new ValidationContext(category, null, null);

            var results = new List<ValidationResult>();
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Category), typeof(Category)), typeof(Category));
            var isModelStateValid = Validator.TryValidateObject(category, context, results, true);

            if (!isModelStateValid)
                target.ModelState.AddModelError("", "error");

            ActionResult result = target.Edit(category).Result;

            mock.Verify(m => m.SaveAsync(category), Times.Never);
            Assert.IsFalse(isModelStateValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Categories()
        {
            Category category = new Category { ID = 1, Name = "First", Alias = "First", isActive = true };
            Category[] categories = new Category[] {
                category,
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            };

            Mock<ICategoryRepository> mock = new Mock<ICategoryRepository>();
            mock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(categories.ElementAtOrDefault(id - 1)); });


            CategoryController target = new CategoryController(mock.Object);

            ActionResult result = target.Delete(category.ID).Result;

            mock.Verify(m => m.FindAsync(category.ID));

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
