﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Threading.Tasks;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;
using RevBlog.Web.Areas.Admin.Controllers;
using Moq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace RevBlog.Tests.Admin.Controllers
{
    [TestClass]
    public class PostControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            //Arrange
            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(r => r.List).Returns(new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
            }.AsQueryable<Post>());

            Mock<ICategoryRepository> catMock = new Mock<ICategoryRepository>();
            catMock.Setup(r => r.List).Returns(new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            }.AsQueryable<Category>());

            Mock<ITagRepository> tagMock = new Mock<ITagRepository>();
            tagMock.Setup(r => r.List).Returns(new Tag[] {

            }.AsQueryable<Tag>());

            //Act

            PostController target = new PostController(postMock.Object, catMock.Object, tagMock.Object);

            Post[] result = ((IEnumerable<Post>) (target.Index().Model)).ToArray();

            //Assert

            Assert.AreEqual(result[0].Title, "Third");
            Assert.AreEqual(result[1].Title, "Second");
            Assert.AreEqual(result[2].Title, "First");
            Assert.AreEqual(result.Length, 3);
        }
        
        [TestMethod]
        public void Can_Edit_Post()
        {
            Post[] posts = new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
            };

            Category[] categories = new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(posts[id - 1]); });

            Mock<ICategoryRepository> catMosk = new Mock<ICategoryRepository>();
            catMosk.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(categories[id - 1]); });

            PostController target = new PostController(postMock.Object, catMosk.Object, null);

            var result1 = target.Edit(1).Result;
            Post p1 = (result1 as ViewResult).ViewData.Model as Post;

            var result2 = target.Edit(2).Result;
            Post p2 = (result2 as ViewResult).ViewData.Model as Post;

            var result3 = target.Edit(3).Result;
            Post p3 = (result3 as ViewResult).ViewData.Model as Post;


            // Assert
            Assert.AreEqual(1, p1.ID);
            Assert.AreEqual(2, p2.ID);
            Assert.AreEqual(3, p3.ID);
        }
        
        [TestMethod]
        public void Cannot_Edit_Non_Existing_Post()
        {
            Post[] posts = new Post[] {
                new Post { ID = 1, Title = "First", Alias = "First", isActive = true },
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
            };

            Category[] categories = new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(posts.ElementAtOrDefault(id - 1)); });

            Mock<ICategoryRepository> catMosk = new Mock<ICategoryRepository>();
            catMosk.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(categories.ElementAtOrDefault(id - 1)); });

            PostController target = new PostController(postMock.Object, catMosk.Object, null);

            var result = target.Edit(4).Result;
            int code = (result as HttpNotFoundResult).StatusCode;

            Assert.AreEqual(404, code);
        }
        
        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock<IPostRepository> postMock = new Mock<IPostRepository>();

            Mock<ICategoryRepository> catMosk = new Mock<ICategoryRepository>();

            PostController target = new PostController(postMock.Object, catMosk.Object, null);

            Post post = new Post { ID=1, Title="First", Alias="First", Description = "First", Content = "First", CategoryID = 1 };

            var context = new ValidationContext(post, null, null);

            var results = new List<ValidationResult>();
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Post), typeof(Post)), typeof(Post));
            var isModelStateValid = Validator.TryValidateObject(post, context, results, true);

            if (!isModelStateValid)
                target.ModelState.AddModelError("", "error");

            ActionResult result = target.Edit(post).Result;
            
            postMock.Verify(m => m.SaveAsync(post));
            Assert.IsTrue(isModelStateValid);
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }
        
        [TestMethod]
        public void Cant_Save_Invalid_Changes()
        {

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();

            Mock<ICategoryRepository> catMock = new Mock<ICategoryRepository>();
            catMock.Setup(r => r.List).Returns(new Category[] {
                new Category { ID = 1, Name = "First", Alias = "First", isActive = true },
                new Category { ID = 2, Name = "Second", Alias = "Second", isActive = true },
                new Category { ID = 3, Name = "Third", Alias = "Third", isActive = true },
            }.AsQueryable<Category>());

            PostController target = new PostController(postMock.Object, catMock.Object, null);

            Post post = new Post { ID = 9999, CategoryID = 0};

            var context = new ValidationContext(post, null, null);

            var results = new List<ValidationResult>();
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Post), typeof(Post)), typeof(Post));
            var isModelStateValid = Validator.TryValidateObject(post, context, results, true);

            if (!isModelStateValid)
                target.ModelState.AddModelError("", "error");

            ActionResult result = target.Edit(post).Result;

            postMock.Verify(m => m.SaveAsync(post), Times.Never);
            Assert.IsFalse(isModelStateValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Post()
        {
            Post post = new Post { ID = 1, Title = "First", Alias = "First", isActive = true };
            Post[] posts = new Post[] {
                post,
                new Post { ID = 2, Title = "Second", Alias = "Second", isActive = true },
                new Post { ID = 3, Title = "Third", Alias = "Third", isActive = true },
            };

            Mock<IPostRepository> mock = new Mock<IPostRepository>();
            mock.Setup(m => m.FindAsync(It.IsAny<int>())).Returns((int id) => { return Task.FromResult(posts.ElementAtOrDefault(id - 1)); });


            PostController target = new PostController(mock.Object, null, null);

            ActionResult result = target.Delete(post.ID).Result;

            mock.Verify(m => m.FindAsync(post.ID));

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Add_Tag()
        {
            Post post = new Post { ID = 1, Title = "First", Alias = "First", isActive = true, Tags = new List<Tag>() };
            Tag tag = new Tag { ID = 1, Name = "TestTag" };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns(Task.FromResult(post));


            Mock<ITagRepository> tagMosk = new Mock<ITagRepository>();
            tagMosk.Setup(m => m.FindByNameAsync(It.IsAny<string>())).Returns(Task.FromResult(tag));

            PostController target = new PostController(postMock.Object, null, tagMosk.Object);

            ActionResult result = target.AddTag(post.ID, tag.Name).Result;

            postMock.Verify(m => m.FindWithTagsAsync(post.ID));

            tagMosk.Verify(m => m.FindByNameAsync(tag.Name));

            postMock.Verify(m => m.SaveAsync(post));

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Can_Remove_Tag()
        {
            Tag tag = new Tag { ID = 1, Name = "TestTag" };

            Post post = new Post { ID = 1, Title = "First", Alias = "First", isActive = true, Tags = new List<Tag> { tag } };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns(Task.FromResult(post));

            PostController target = new PostController(postMock.Object, null, null);

            ActionResult result = target.DeleteTag(post.ID, tag.ID).Result;

            postMock.Verify(m => m.FindWithTagsAsync(post.ID));

            postMock.Verify(m => m.SaveAsync(post));

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Cannot_Remove_Nonexisting_Tag()
        {
            Tag tag = new Tag { ID = 1, Name = "TestTag" };

            Post post = new Post { ID = 1, Title = "First", Alias = "First", isActive = true, Tags = new List<Tag> { tag } };

            Mock<IPostRepository> postMock = new Mock<IPostRepository>();
            postMock.Setup(m => m.FindWithTagsAsync(It.IsAny<int>())).Returns(Task.FromResult(post));

            PostController target = new PostController(postMock.Object, null, null);

            ActionResult result = target.DeleteTag(post.ID, tag.ID + 2).Result;

            postMock.Verify(m => m.FindWithTagsAsync(post.ID));

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }
    }
}
