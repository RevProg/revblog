﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Moq;
using System.Web.Routing;

namespace RevBlog.Tests.Components
{
    static class MoqComponents
    {
        public static ControllerContext GetContext(ControllerBase controller)
        {
            var server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);
            var response = new Mock<HttpResponseBase>(MockBehavior.Strict);
            var request = new Mock<HttpRequestBase>(MockBehavior.Strict);
            request.Setup(x => x.ApplicationPath).Returns("/");
            request.Setup(x => x.Url).Returns(new Uri("http://localhost"));
            request.Setup(x => x.ServerVariables).Returns(new System.Collections.Specialized.NameValueCollection{
                { "SERVER_NAME", "localhost" },
                { "SCRIPT_NAME", "localhost" },
                { "SERVER_PORT", "80" },
                { "HTTPS", "www.revblog.com" },
                { "REMOTE_ADDR", "127.0.0.1" },
                { "REMOTE_HOST", "127.0.0.1" }
            });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            context.SetupGet(x => x.Response).Returns(response.Object);
            context.SetupGet(x => x.Server).Returns(server.Object);

            return new ControllerContext(context.Object, new RouteData(), controller);
        }

        public static UrlHelper GetUrl()
        {
            var url = new Mock<UrlHelper>(MockBehavior.Strict);
            url.Setup(x => x.Action(It.IsAny<string>(), It.IsAny<string>())).Returns((string action, string controller) => controller + "/" + action);
            url.Setup(x => x.Action(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>())).Returns((string action, string controller, object routeValues) => controller + "/" + action + "/1-first" );

            return url.Object;
        }
    }
}
