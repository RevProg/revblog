﻿using System.Linq;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;

namespace RevBlog.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository repository;

        public CategoryController(ICategoryRepository repo)
        {
            repository = repo;
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600)]
        // GET: Category/List
        public PartialViewResult List()
        {
            return PartialView(repository.List.Where(c => c.isActive));
        }
    }
}