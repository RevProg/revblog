﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;

namespace RevBlog.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostRepository repository;

        public HomeController(IPostRepository repo)
        {
            repository = repo;
        }

        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index()
        {
            var posts = repository.List
                .Where(p => p.isActive)
                .Include(p => p.Category)
                .Include(p => p.Tags)
                .OrderByDescending(p => p.ID)
                .Take(5)
                .ToList();

            return View(posts);
        }

        [Route("About")]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult About()
        {
            return View();
        }

        [Route("Contact")]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}