﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Entities;

namespace RevBlog.Web.Controllers
{
    public class TagController : Controller
    {
        readonly ITagRepository _repository;

        public TagController(ITagRepository repo)
        {
            _repository = repo;
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600)]
        // GET: Tag/List
        public PartialViewResult List()
        {
            return PartialView(_repository.List.OrderByDescending(t => t.Posts.Count()));
        }
    }
}