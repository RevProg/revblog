﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RevBlog.Web.Attributes;

namespace RevBlog.Web.Controllers
{
    [NoCache]
    public class NavController : Controller
    {
        // GET: Nav
        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            return PartialView();
        }
    }
}