﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RevBlog.Domain.Abstract;
using RevBlog.Web.Extensions.BreadCrumbs;
using RevBlog.Web.Models;

namespace RevBlog.Web.Controllers
{
    public class BlogController : Controller
    {
        private readonly BreadCrumb breadCrumb;
        private readonly ICategoryRepository catRepository;
        public int ItemsPerPage = 10;

        private readonly IPostRepository postRepository;
        private readonly ITagRepository tagRepository;

        public BlogController(IPostRepository postRepo, ICategoryRepository catRepo, ITagRepository tagRepo)
        {
            postRepository = postRepo;
            catRepository = catRepo;
            tagRepository = tagRepo;

            breadCrumb = new BreadCrumb("breadcrumb pull-right");
        }

        // GET: Blog
        [OutputCache(Duration = 3600, VaryByParam = "page")]
        public ActionResult Index(int page = 1)
        {
            var model = new PostListViewModel
            {
                Posts = postRepository.List
                    .Where(p => p.isActive)
                    .Include(p => p.Category)
                    .Include(p => p.Tags)
                    .OrderByDescending(p => p.ID)
                    .Skip((page - 1)*ItemsPerPage)
                    .Take(ItemsPerPage),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = ItemsPerPage,
                    TotalItems = postRepository.List.Count(p => p.isActive)
                }
            };

            if ((model.PagingInfo.TotalPages > 0) && (model.PagingInfo.TotalPages < page))
                return HttpNotFound();

            breadCrumb.Add(new Node {Title = "Блог", Url = Request.Url.LocalPath});
            ViewBag.breadCrumb = breadCrumb;

            return View(model);
        }

        public ActionResult Search(string q, int page = 1)
        {
            if (q.Length == 0)
                throw new HttpException(400, "Bad Request");

            var model = new SearchListViewModel
            {
                Posts = postRepository.List
                    .Where(p => p.isActive && (p.Title.ToLower().Contains(q) || p.Description.ToLower().Contains(q)))
                    .Include(p => p.Category)
                    .Include(p => p.Tags)
                    .OrderByDescending(p => p.ID)
                    .Skip((page - 1)*ItemsPerPage)
                    .Take(ItemsPerPage),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = ItemsPerPage,
                    TotalItems =
                        postRepository.List
                            .Count(p => p.isActive && (p.Title.ToLower().Contains(q) || p.Description.ToLower().Contains(q)))
                },
                Query = q
            };

            breadCrumb.Add(new Node {Title = "Блог", Url = Url.Action("Index", "Blog")});
            breadCrumb.Add(new Node {Title = "Результаты поиска: " + q, Url = Request.Url.LocalPath});
            ViewBag.breadCrumb = breadCrumb;

            return View(model);
        }

        // GET: Blog/Category/4-alias
        [OutputCache(Duration = 3600, VaryByParam = "id;alias;page")]
        public async Task<ActionResult> Category(int id, string alias, int page = 1)
        {
            var category = await catRepository.FindAsync(id);

            if ((category == null) || !category.isActive)
                return HttpNotFound();

            var model = new PostListViewModel
            {
                Posts = postRepository.List
                    .Where(p => (p.CategoryID == category.ID) && p.isActive)
                    .Include(p => p.Category)
                    .Include(p => p.Tags)
                    .OrderByDescending(p => p.ID)
                    .Skip((page - 1)*ItemsPerPage)
                    .Take(ItemsPerPage),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = ItemsPerPage,
                    TotalItems = postRepository.List.Count(p => (p.CategoryID == category.ID) && p.isActive)
                },
                Title = category.Name
            };

            if ((model.PagingInfo.TotalPages > 0) && (model.PagingInfo.TotalPages < page))
                return HttpNotFound();

            breadCrumb.Add(new Node {Title = "Блог", Url = Url.Action("Index", "Blog")});
            breadCrumb.Add(new Node {Title = model.Title, Url = Request.Url.LocalPath});
            ViewBag.breadCrumb = breadCrumb;

            return View("Index", model);
        }


        // GET: Blog/Category/4-alias
        [OutputCache(Duration = 3600, VaryByParam = "name;page")]
        public async Task<ActionResult> Tag(string name, int page = 1)
        {
            var tag = await tagRepository.FindByNameAsync(name);

            if (tag == null)
                return HttpNotFound();

            var model = new PostListViewModel
            {
                Posts = postRepository.List
                    .Where(p => p.isActive && p.Tags.Any(t => t.ID == tag.ID))
                    .Include(p => p.Category)
                    .OrderByDescending(p => p.ID)
                    .Skip((page - 1)*ItemsPerPage)
                    .Take(ItemsPerPage),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = ItemsPerPage,
                    TotalItems = postRepository.List.Count(p => p.isActive && p.Tags.Any(t => t.ID == tag.ID))
                },
                Title = tag.Name
            };

            if ((model.PagingInfo.TotalPages > 0) && (model.PagingInfo.TotalPages < page))
                return HttpNotFound();

            breadCrumb.Add(new Node {Title = "Блог", Url = Url.Action("Index", "Blog")});
            breadCrumb.Add(new Node {Title = "Тег: " + model.Title, Url = Request.Url.LocalPath});
            ViewBag.breadCrumb = breadCrumb;

            return View("Index", model);
        }

        // GET: Blog/5-alias
        [OutputCache(Duration = 3600, VaryByParam = "id;alias")]
        public async Task<ActionResult> Details(int id, string alias)
        {
            var post = await postRepository.FindWithTagsAsync(id);

            if ((post == null) || !post.isActive || (post.Alias != alias))
                return HttpNotFound();

            breadCrumb.Add(new Node {Title = "Блог", Url = Url.Action("Index", "Blog")});
            breadCrumb.Add(new Node
            {
                Title = post.Category.Name,
                Url = Url.Action("Category", "Blog", new {id = post.Category.ID, alias = post.Category.Alias})
            });
            breadCrumb.Add(new Node {Title = post.Title, Url = Request.Url.LocalPath});
            ViewBag.breadCrumb = breadCrumb;

            return View(post);
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600)]
        // GET: Tag/List
        public PartialViewResult LastPostsList(int count = 5)
        {
            return PartialView(postRepository.List.Where(p => p.isActive).OrderByDescending(t => t.Created).Take(count));
        }
    }
}