﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RevBlog.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true;

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Blog_Item",
                url: "blog/{id}-{alias}",
                defaults: new { controller = "Blog", action = "Details", },
                namespaces: new[] { "RevBlog.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Blog_Category",
                url: "blog/category/{id}-{alias}",
                defaults: new { controller = "Blog", action = "Category", },
                namespaces: new[] { "RevBlog.Web.Controllers" }
            );


            routes.MapRoute(
                name: "Blog_Tag",
                url: "blog/tag/{name}",
                defaults: new { controller = "Blog", action = "Tag", },
                namespaces: new[] { "RevBlog.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "RevBlog.Web.Controllers" }
            );
        }
    }
}
