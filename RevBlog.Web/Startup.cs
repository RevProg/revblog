﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RevBlog.Web.Startup))]
namespace RevBlog.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
