﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Concrete;

namespace RevBlog.Web.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<IPostRepository>().To<PostRepository>();
            kernel.Bind<ITagRepository>().To<TagRepository>();
        }
    }
}