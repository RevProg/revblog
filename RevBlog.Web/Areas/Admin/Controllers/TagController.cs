﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RevBlog.Domain.Entities;
using RevBlog.Web.Models;
using RevBlog.Domain.Abstract;

namespace RevBlog.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class TagController : Controller
    {
        private ITagRepository repository;

        public TagController(ITagRepository repo)
        {
            repository = repo;
        }

        // GET: Admin/Tag
        public ViewResult Index()
        {
            return View(repository.List.ToList());
        }
        
        // GET: Admin/Tag/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tag tag = await repository.FindAsync(id.Value);

            if (tag == null)
            {
                return HttpNotFound();
            }

            return View(tag);
        }

        // GET: Admin/Tag/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Tag/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name")] Tag tag)
        {
            if (ModelState.IsValid)
            {
                await repository.SaveAsync(tag);
                return RedirectToAction("Index");
            }

            return View(tag);
        }

        // GET: Admin/Tag/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tag tag = await repository.FindAsync(id.Value);

            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        // POST: Admin/Tag/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name")] Tag tag)
        {
            if (ModelState.IsValid)
            {
                await repository.SaveAsync(tag);
                return RedirectToAction("Index");
            }
            return View(tag);
        }

        // GET: Admin/Tag/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tag tag = await repository.FindAsync(id.Value);

            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        // POST: Admin/Tag/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await repository.DeleteAsync(id);

            return RedirectToAction("Index");
        }
    }
}
