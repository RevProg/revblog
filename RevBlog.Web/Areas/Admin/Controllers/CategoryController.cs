﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RevBlog.Domain.Entities;
using RevBlog.Domain.Abstract;
using RevBlog.Domain.Concrete;

namespace RevBlog.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private ICategoryRepository repository;

        public CategoryController(ICategoryRepository repo)
        {
            repository = repo;
        }

        // GET: Admin/Category
        public ActionResult Index()
        {
            var categories = repository.List.Where(c => c.ParentID == null);
            return View(categories);
        }
        
        // GET: Admin/Category/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = await repository.FindAsync(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // GET: Admin/Category/Create
        public ActionResult Create()
        {
            ViewBag.ParentID = new SelectList(repository.List.Where(c => c.ParentID == null), "ID", "Name");
            return View();
        }

        // POST: Admin/Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Alias,ParentID,Description,MetaDescription,MetaKeyWords,isActive")] Category category)
        {
            if (ModelState.IsValid)
            {
                await repository.SaveAsync(category);
                return RedirectToAction("Index");
            }

            ViewBag.ParentID = new SelectList(repository.List.Where(c => c.ParentID == null), "ID", "Name");
            return View(category);
        }
        
        // GET: Admin/Category/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = await repository.FindAsync(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }

            ViewBag.ParentID = new SelectList(repository.List.Where(c => c.ID != id.Value && c.ParentID == null), "ID", "Name", category.ParentID);
            return View(category);
        }

        // POST: Admin/Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Alias,ParentID,Description,MetaDescription,MetaKeyWords,isActive")] Category category)
        {
            if (ModelState.IsValid)
            {
                await repository.SaveAsync(category);
                return RedirectToAction("Index");
            }
            ViewBag.ParentID = new SelectList(repository.List.Where(c => c.ID != category.ID && c.ParentID == null), "ID", "Name", category.ParentID);
            return View(category);
        }

        // GET: Admin/Category/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = await repository.FindAsync(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // POST: Admin/Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await repository.DeleteAsync(id);

            return RedirectToAction("Index");
        }
    }
}
