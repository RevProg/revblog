﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RevBlog.Domain.Entities;
using RevBlog.Domain.Abstract;
using RevBlog.Web.Models;

namespace RevBlog.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private IPostRepository postRepository;
        private ICategoryRepository categoryRepository;
        private ITagRepository tagRepository;

        public PostController(IPostRepository postRepo, ICategoryRepository catRepo, ITagRepository tagRepo)
        {
            postRepository = postRepo;
            categoryRepository = catRepo;
            tagRepository = tagRepo;
        }

        // GET: Admin/Post
        public ViewResult Index()
        {
            return View(postRepository.List.Include(p => p.Category).OrderByDescending(p => p.ID).ToList());
        }
        
        // GET: Admin/Post/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Post post = await postRepository.FindWithTagsAsync(id.Value);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(post);
        }

        // GET: Admin/Post/Create
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(categoryRepository.List, "ID", "Name");

            return View();
        }

        // POST: Admin/Post/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Title,Alias,CategoryID,Description,Content,MetaDescription,MetaKeyWords,isActive,Created")] Post post)
        {
            post.Created = DateTime.Now;

            if (ModelState.IsValid)
            {
                await postRepository.SaveAsync(post);
                return RedirectToAction("Details", new { id = post.ID });
            }

            ViewBag.CategoryList = new SelectList(categoryRepository.List, "ID", "Name");

            return View(post);
        }

        // GET: Admin/Post/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Post post = await postRepository.FindAsync(id.Value);

            if (post == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryList = new SelectList(categoryRepository.List, "ID", "Name");

            return View(post);
        }

        // POST: Admin/Post/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Title,Alias,CategoryID,Description,Content,MetaDescription,MetaKeyWords,isActive,Created")] Post post)
        {
            if (ModelState.IsValid)
            {
                await postRepository.SaveAsync(post);
                return RedirectToAction("Details", new { id = post.ID });
            }

            ViewBag.CategoryList = new SelectList(categoryRepository.List, "ID", "Name");

            return View(post);
        }

        //POST: Admin/Post/AddTag/5
        [HttpPost]
        public async Task<ActionResult> AddTag(int id, string name)
        {
            Post post = await postRepository.FindWithTagsAsync(id);

            if (post == null || name == null)
                return HttpNotFound();

            Tag tag = await tagRepository.FindByNameAsync(name);

            if (tag == null)
            {
                tag = new Tag { Name = name };
                await tagRepository.SaveAsync(tag);
            }
            else
            {
                bool exist = post.Tags.Where(t => t.ID == tag.ID).Count() > 0;

                if (exist)
                    return RedirectToAction("Details", new { id = id });

            }

            post.Tags.Add(tag);

            await postRepository.SaveAsync(post);
                
            return RedirectToAction("Details", new { id = id });
        }

        //POST: Admin/Post/AddTag/5
        [HttpPost]
        public async Task<ActionResult> DeleteTag(int id, int tagID)
        {
            Post post = await postRepository.FindWithTagsAsync(id);
            Tag tag = post.Tags.Where(t => t.ID == tagID).SingleOrDefault();

            if (post == null || tag == null)
                return HttpNotFound();

            post.Tags.Remove(tag);

            await postRepository.SaveAsync(post);
            return RedirectToAction("Details", new { id = id });
        }

        // GET: Admin/Post/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Post post = await postRepository.FindAsync(id.Value);

            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Admin/Post/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await postRepository.DeleteAsync(id);

            return RedirectToAction("Index");
        }
    }
}
