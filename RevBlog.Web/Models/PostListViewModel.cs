﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RevBlog.Domain.Entities;

namespace RevBlog.Web.Models
{
    public class PostListViewModel
    {
        public IEnumerable<Post> Posts { get; set; }

        public PagingInfo PagingInfo { get; set; }

        public string Title { get; set; }
    }
}