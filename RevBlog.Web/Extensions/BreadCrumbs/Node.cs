﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RevBlog.Web.Extensions.BreadCrumbs
{
    public class Node
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}