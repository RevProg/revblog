﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace RevBlog.Web.Extensions.BreadCrumbs
{
    public class BreadCrumb
    {
        protected List<Node> nodes;
        protected string listClass;

        public bool UseSchema { get; set;
        }
        public BreadCrumb()
        {
            nodes = new List<Node>();
            listClass = "breadcrumb";
            UseSchema = true;

            nodes.Add(new Node { Title = "Главная", Url = "/"});
        }

        public BreadCrumb(string listClass): this()
        {
            this.listClass = listClass;
        }

        public BreadCrumb(bool useSchema) : this()
        {
            UseSchema = useSchema;
        }

        public BreadCrumb(string listClass, bool useSchema) : this()
        {
            this.listClass = listClass;
            UseSchema = useSchema;
        }

        public void Add(Node node)
        {
            nodes.Add(node);
        }

        public void AddRange(Node[] nodes)
        {
            this.nodes.AddRange(nodes);
        }

        public List<Node> Nodes
        {
            get { return nodes; }
        }

        public int Count
        {
            get { return nodes.Count; }
        }

        public MvcHtmlString Render()
        {
            if (nodes.Count == 1)
                return MvcHtmlString.Empty;

            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass(listClass);
            if (UseSchema)
            {
                ul.MergeAttribute("itemscope", String.Empty);
                ul.MergeAttribute("itemtype", "http://schema.org/BreadcrumbList");
            }

            StringBuilder items = new StringBuilder();
            Node last = nodes.Last();
            foreach(Node item in nodes)
            {
                TagBuilder li = new TagBuilder("li");
                if (UseSchema)
                {
                    li.MergeAttribute("itemprop", "itemListElement");
                    li.MergeAttribute("itemtype", "http://schema.org/ListItem");
                    li.MergeAttribute("itemscope", String.Empty);
                }

                if (item.Equals(last))
                {
                    li.AddCssClass("active");
                    li.InnerHtml = item.Title;
                }
                else
                {
                    TagBuilder a = new TagBuilder("a");
                    a.MergeAttribute("href", item.Url);
                    a.InnerHtml = item.Title;
                    li.InnerHtml = a.ToString();
                }

                items.Append(li.ToString());
            }

            ul.InnerHtml = items.ToString();

            return MvcHtmlString.Create(ul.ToString());
        }
    }
}