﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace RevBlog.Web.Extensions
{
    public static class MenuExtension
    {
        public static MvcHtmlString MenuItem(this HtmlHelper htmlHelper, string text, string action, string controller, string area = null)
        {
            var li = new TagBuilder("li");

            var routeData = htmlHelper.ViewContext.RouteData;

            var currentAction = routeData.GetRequiredString("action");
            var currentController = routeData.GetRequiredString("controller");
            var currentArea = routeData.DataTokens["area"] as string;

            if (string.Equals(currentAction, action, StringComparison.OrdinalIgnoreCase) &&
            string.Equals(currentController, controller, StringComparison.OrdinalIgnoreCase) &&
            string.Equals(currentArea, area, StringComparison.OrdinalIgnoreCase))
            {
                li.AddCssClass("active");
            }

            li.InnerHtml = htmlHelper.ActionLink(text, action, controller, new { area }, null).ToHtmlString();

            return MvcHtmlString.Create(li.ToString());
        }
    }
}