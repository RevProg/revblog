﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RevBlog.Web.Models;

namespace RevBlog.Web.Extensions {
    public static class PagingHelpers {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");

            StringBuilder items = new StringBuilder();

            for (int i = 1; i <= pagingInfo.TotalPages; i++) {
                TagBuilder li = new TagBuilder("li");
                TagBuilder a = new TagBuilder("a");
                a.MergeAttribute("href", pageUrl(i));
                a.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage) {
                    li.AddCssClass("active");
                }
                li.InnerHtml = a.ToString();
                items.Append(li.ToString());
            }

            ul.InnerHtml = items.ToString();

            return MvcHtmlString.Create(ul.ToString());
        }
    }
}