namespace RevBlog.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Post_Created_Field : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "Created");
        }
    }
}
