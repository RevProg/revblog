using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RevBlog.Domain.Concrete;
using RevBlog.Web.Models;

namespace RevBlog.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RevBlog.Domain.Concrete.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\Identity";
            ContextKey = "RevBlog.Web.Models.ApplicationDbContext";
        }

        protected override void Seed(BlogContext context)
        {

            if (!(context.Users.Any(u => u.UserName == "revavg93@gmail.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var user = new ApplicationUser { UserName = "revavg93@gmail.com", Email = "revavg93@gmail.com" };
                userManager.Create(user, "2535164");
            }
        }
    }
}
