namespace RevBlog.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Nullable_From_Entities : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "isActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Posts", "isActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "isActive", c => c.Boolean());
            AlterColumn("dbo.Categories", "isActive", c => c.Boolean());
        }
    }
}
