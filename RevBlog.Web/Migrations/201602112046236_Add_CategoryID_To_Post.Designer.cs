// <auto-generated />
namespace RevBlog.Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_CategoryID_To_Post : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_CategoryID_To_Post));
        
        string IMigrationMetadata.Id
        {
            get { return "201602112046236_Add_CategoryID_To_Post"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
